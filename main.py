import re


def address_extractor_phone(input_sentence):
    z = re.findall(r'[\+\(0۰]?[1-9۱-۹][0-9۰-۹ .\-\(\)]{6,}[0-9۰-۹]', input_sentence)
    return z


def address_extractor_email(input_sentence):
    z = re.findall(r'([a-zA-Z0-9._-]+[@][a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]{2,})', input_sentence)
    return z


def address_extractor_website(input_sentence):
    z = re.findall(r'(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-&?=%.]+', input_sentence)
    return z


text_phone = """با شماره‌ی همراه من ۰۹۳۵۵۶۷۳۳۷۲ تماس بگیرید.
و در صورتی که پاسخ‌گو نبودم می‌توانید با شماره دیگر من +989123333333 تماس بگیرید
و در صورتی که آن را هم جواب ندادم با هر کدام از شماره‌های  +۹۸۹۳۵-۱۲۳۱۲۳۴ یا +98(935)1231234 و در نهایت ۰۹۳۵ ۱۲۳۱۲۳۱۲ تماس بگیرید
"""

text_email = """
این ایمیل را نباید بخونه ali@gmail.c ولی ایمیل رو باید بخونه ali@gmail.com همچنین ایمیل با چند پسوند نباید مشکل داشته باشه ali@gmail.co.ca و در نهایت ایمیل با 
نقطه باید اوکی باشه ali.amiri@gmail.com
"""

text_url = """ ali@gmail.com 
Hello www.google.com World http://yahoo.com or ftp://sharif.edu https://varzesh3.com and digikala.com
"""

output = address_extractor_phone(text_phone)

print(output)

output = address_extractor_email(text_email)
print(output)

output = address_extractor_website(text_url)
print(output)
